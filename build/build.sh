#!/bin/bash

# script taken from https://gist.github.com/mshafiee/5a681bbefda8f26f1f257d62f5e4a699

#run in root dir eg. ./build/build.sh <command>

echo "get source from facebook: git clone https://github.com/facebook/time.git"
echo "add version to source in main.go (eg. fmt.Println(\"gontpresponder version vYYYY.MM.DD.HH\") )"

echo "What version? (no v, just numbers with periods)"
read version

BIN_FILE_NAME_PREFIX=$1
PROJECT_DIR=$PWD
PLATFORMS=$(go tool dist list)
#PLATFORMS="linux/amd64 linux/arm \
#	   darwin/amd64 \
#	   freebsd/amd64 freebsd/arm \
#	   netbsd/amd64 netbsd/arm \
#	   openbsd/amd64 openbsd/arm \
#	   windows/amd64"

#apt update
#apt install -y zip

FILEPATH="$PROJECT_DIR/bin"
mkdir -p $FILEPATH
cat /dev/null > $FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt

for PLATFORM in $PLATFORMS; do
        GOOS=${PLATFORM%/*}
        GOARCH=${PLATFORM#*/}
        #FILEPATH="$PROJECT_DIR/bin"
        ##echo $FILEPATH
        #mkdir -p $FILEPATH
        BIN_FILE_NAME="$FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}_${GOOS}_${GOARCH}"
        #echo $BIN_FILE_NAME
        if [ "${GOOS}" = "windows" ]; then BIN_FILE_NAME="${BIN_FILE_NAME}.exe"; fi
        CMD="CGO_ENABLED=0 GOOS=${GOOS} GOARCH=${GOARCH} go build -ldflags '-w -s' -o ${BIN_FILE_NAME}"
        #echo $CMD
        echo "${CMD}"
        eval $CMD || FAILURES="${FAILURES} ${PLATFORM}"

        if [ -f "${BIN_FILE_NAME}" ]; then
        echo "md5sum `md5sum ${BIN_FILE_NAME}`" >> $FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt
        echo "sha1sum `sha1sum ${BIN_FILE_NAME}`" >> $FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt
        echo "sha256sum `sha256sum ${BIN_FILE_NAME}`" >> $FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt
        echo "sha384sum `sha384sum ${BIN_FILE_NAME}`" >> $FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt
        echo "sha512sum `sha512sum ${BIN_FILE_NAME}`" >> $FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt
        fi

	#if [[ "${GOOS}" == "windows" ]]; then
	    #zip $FILEPATH/${BIN_FILE_NAME_PREFIX}-${GOOS}-${GOARCH}.zip -j ${BIN_FILE_NAME} LICENSE README.md
	    #rm $FILEPATH/${BIN_FILE_NAME_PREFIX}.exe
	#else
	    #cp README.md LICENSE doc/${BIN_FILE_NAME_PREFIX}.man $FILEPATH
	    #tar czvf $FILEPATH/${BIN_FILE_NAME_PREFIX}-${GOOS}-${GOARCH}.tar.gz -C $FILEPATH ${BIN_FILE_NAME_PREFIX} README.md LICENSE ${BIN_FILE_NAME_PREFIX}.man
	    #rm $FILEPATH/${BIN_FILE_NAME_PREFIX} $FILEPATH/README.md $FILEPATH/LICENSE $FILEPATH/${BIN_FILE_NAME_PREFIX}.man
	#fi
done

echo "removing filepath from sum"
sed -i 's,'"${FILEPATH}/"',,g' "$FILEPATH/${BIN_FILE_NAME_PREFIX}_v${version}.sum.txt"
